-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.34-log - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица my_blog.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id_article` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_author` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT 'Статья',
  `content` text NOT NULL,
  `publication_date` timestamp NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_article`),
  KEY `id_author` (`id_author`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`id_author`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы my_blog.articles: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
REPLACE INTO `articles` (`id_article`, `id_author`, `name`, `content`, `publication_date`, `last_modified`, `is_delete`) VALUES
	(1, 14, 'html-5', 'Последняя версия стандарта HTML. Термин имеет два определения: Новая версия языка HTML, с новыми элементами, атрибутами и новым поведением. Набор технологий, позволяющий создавать разнообразные сайты и Web-приложения', '2017-12-12 20:41:41', '2017-12-12 20:41:41', 0),
	(2, 8, 'php-7', 'В 2014 году было проведено голосование, по результатам которого следующая версия получила название PHP 7[19]. Выход новой версии планировался в середине октября 2015 года[20]. В марте 2015 года Zend представили инфографику в которой описаны основные нововведения PHP 7[21].\r\n3 декабря 2015 года было объявлено о выходе PHP версии 7.0.0.\r\nНовая версия основывается на экспериментальной ветви PHP, которая изначально называлась phpng (PHP Next Generation — следующее поколение), и разрабатывалась с упором на увеличение производительности и уменьшение потребления памяти[23]. В новой версии добавлена возможность указывать тип возвращаемых из функции данных[24], добавлен контроль передаваемых типов для скалярных данных[25], а также новые операторы.', '2017-12-12 20:41:41', '2017-12-24 21:02:30', 0),
	(3, 16, 'vue-js', 'Прогрессивный JavaScript-фреймворк с открытым исходным кодом для создания пользовательских интерфейсов.[4] Является реализацией идей реактивного программирования. Легко интегрируется в проекты с использованием других JavaScript-библиотек, благодаря постепенно внедряемой экосистеме.[5] Vue может функционировать как web-фреймворк, помогающий разрабатывать продвинутые одностраничные приложения.  ', '2017-12-12 20:46:47', '2017-12-17 20:14:16', 1),
	(4, 8, 'Yii', 'Объектно-ориентированный компонентный фреймворк, написанный на PHP и реализующий парадигму MVC[3].', '2017-12-12 20:46:47', '2017-12-24 21:34:49', 0),
	(5, 18, 'Laravel', 'Бесплатный веб-фреймворк с открытым кодом, предназначенный для разработки с использованием архитектурной модели MVC (англ. Model View Controller — модель-представление-контроллер). Laravel выпущен под лицензией MIT. Исходный код проекта размещается на GitHub[2].\r\n\r\nВ результате опроса sitepoint.com в декабре 2013 года о самых популярных PHP-фреймворках Laravel занял место самого многообещающего проекта на 2014 год[3].\r\n\r\nВ 2015 году в результате опроса sitepoint.com по использованию PHP-фреймворков среди программистов занял первое место в номинациях:\r\nЕщё доп текст!', '2017-12-12 20:48:43', '2017-12-20 23:35:19', 0),
	(7, 8, 'статья', 'содержание статьи', '2017-12-12 21:14:07', '2017-12-17 20:17:51', 1),
	(15, 8, 'Ещё одна новая статья', 'её содержимое.', '2017-12-17 20:20:02', '2017-12-17 20:20:18', 1),
	(19, 9, 'test', 'test', '2018-01-16 21:52:33', '2018-01-16 21:52:33', 0),
	(20, 8, 'ss', 'ssss', '2018-01-29 14:04:01', '2018-02-11 19:39:22', 1);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Дамп структуры для таблица my_blog.auth
CREATE TABLE IF NOT EXISTS `auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(128) DEFAULT '0',
  `id_user` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы my_blog.auth: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth` ENABLE KEYS */;

-- Дамп структуры для таблица my_blog.users
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL,
  `login` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `login` (`login`),
  KEY `users_ibfk_1` (`id_group`),
  KEY `uid` (`uid`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `user_groups` (`id_group`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы my_blog.users: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id_user`, `id_group`, `login`, `password`, `uid`, `first_name`, `last_name`, `phone`, `email`, `is_delete`) VALUES
	(8, 3, 'axfd', 'ea92ab00c087ce22a02fe6273471e4ed18ab5b99b3355a3bb40cf889726dbf7a', '4ed22e6f4fdff7762927c8734d90054f1221bedcebacf8620791800b46a6364b', 'Алексей', 'Федотов', '8-999-99-99-99', 'axfd@mail.com', 0),
	(9, 1, 'admin', '49f1f8963a12966196dcc691f2ea2a77be25fcd5791862667b59aac3c16a5450', 'c5468fe2cfe04c9e98353e056f4f886491e90b2ec291ff167a4a6abcc2d3bdc3', 'Администратор', '', '8-999-99-99-99', 'admin@mail.ru', 0),
	(12, 2, 'editor', '49f1f8963a12966196dcc691f2ea2a77be25fcd5791862667b59aac3c16a5450', '7e6c9030564420ff6d4dbd1e7bb6e77d1bb4861a986668f92f66d637d65e99b1', 'Контент менеджер', '', '8-999-88-88-88', 'editor@mail.ru', 0),
	(14, 3, 'ivan', '6ad6db7fcea4990eb23f958d60fa6b7e94c4550a84913aac34cb33d0e8388b1e', 'bab8355429046d43e0d49e6134c4c09ee5ce5acf13f71fbd9ee7c5cb7dbdd73f', 'Иван', 'Иванов', '8-999-77-77-77', 'inav@gmail.com', 0),
	(16, 3, 'petr', '6ad6db7fcea4990eb23f958d60fa6b7e94c4550a84913aac34cb33d0e8388b1e', 'bb10f4fac2b2860c818ca524b726dd4ee31f81b57cd20cf0bc7f066550344141', 'Петр', 'Петров', '8-999-77-77-55', 'petr@gmail.com', 0),
	(18, 3, 'sidr', '6ad6db7fcea4990eb23f958d60fa6b7e94c4550a84913aac34cb33d0e8388b1e', '52c62676e16ab0dbb5ff8e9f8b1e2842e9c04a15c58e544cb57697028f84b3c3', 'Сидор', 'Сидоров', '8-999-555555', 'sd@mail.ru', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица my_blog.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_level` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы my_blog.user_groups: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
REPLACE INTO `user_groups` (`id_group`, `access_level`, `name`) VALUES
	(1, 100, 'Администратор'),
	(2, 50, 'Контент менеджер'),
	(3, 0, 'Пользователь');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
