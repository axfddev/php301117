<form method="post" class="article-form">
    <div>
        <div>Название</div>
        <input class="article-form__name js-form-item <?= ($err['name']) ? 'error' : '' ?>" type="text"
               name="name"
               value="<?= $name ?>">
    </div>
    <div>
        <div>Контент</div>
        <textarea class="article-form__content js-form-item <?= ($err['content']) ? 'error' : '' ?>"
                  name="content"><?= $content ?></textarea>
    </div>
    <div class="article-form__error-msg"><?= $err['text'] ?></div>
    <input type="hidden" value="<?= $id_article ?>" name="articleId">
    <button class="article-form__btn">Сохранить</button>
</form>