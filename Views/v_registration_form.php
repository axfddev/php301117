<form method="post">
    <div class="form-item">
        Логин<br>
        <input class="<?= ($err['login']) ? 'error' : '' ?> js-form-item" type="text" name="login"
               value="<?= $login ?>">
    </div>
    <div class="form-item">
        Имя<br>
        <input class="<?= ($err['name']) ? 'error' : '' ?> js-form-item" type="text" name="name" value="<?= $name ?>">
    </div>
    <div class="form-item">
        Фамилия<br>
        <input type="text" name="last-name" value="<?= $lastName ?>">
    </div>
    <div class="form-item">
        Телефон<br>
        <input type="text" name="phone" value="<?= $phone ?>">
    </div>
    <div class="form-item">
        Электронная почта<br>
        <input type="text" name="email" value="<?= $email ?>">
    </div>
    <div class="form-item">
        Пароль<br>
        <input class="<?= ($err['password']) ? 'error' : '' ?> js-form-item" type="password" name="password"
               value="<?= $password ?>">
    </div>
    <div class="form-item">
        Повторите пароль<br>
        <input class="<?= ($err['repeat-password']) ? 'error' : '' ?>" type="password" name="repeat-password"
        value="<?= $repeatPassword ?>">
    </div>
    <input type="submit" value="Зарегистрироваться" class="form-item">
    <div class="article-form__error-msg"><?= $err['text'] ?></div>
</form>