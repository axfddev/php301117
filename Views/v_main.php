<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="<?=ROOT?>Assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?=ROOT?>Assets/css/style.css">
    <script src="<?=ROOT?>Assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=ROOT?>Assets/js/main.js"></script>
</head>
<body>
<header class="header">
    <a href="<?=ROOT?>" name="На главную" class="header__to-main"><i class="fa fa-home fa-2x"></i></a>
    <h1 class="header__name"><?= $title ?></h1>
    <? if ($_SESSION['auth']): ?>
        <div class="header__login">Здравствуйте, <?= $_SESSION['user']['name'] ?>! <a href="<?=ROOT?>articles/login?logout=true">выход</a>
        </div>
    <? else: ?>
        <div class="header__login"><a href="<?=ROOT?>articles/login">Авторизоваться</a></div>
    <? endif; ?>
</header>
<div class="wrapper">

    <?= $content ?>

</div>
<div class="modal js-modal">
    <div class="modal__blackout"></div>
    <div class="modal__content js-modal-content"></div>
</div>
</body>
</html>