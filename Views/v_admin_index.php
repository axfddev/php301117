<table class="articles-list">
    <tr>
        <th>Название статьи</th>
        <th class="articles-list__pd">Дата публикации</th>
        <th class="articles-list__pd">Автор</th>
        <th></th>
        <th></th>
    </tr>
    <? foreach ($articles as $article) : ?>
        <tr>
            <td>
                <a href="<?= ROOT ?>articles/article/<?= $article['id_article'] ?>"><?= $article['name'] ?></a>
            </td>
            <td class="articles-list__pd">
                <?= date('d.m.Y', strtotime($article['publication_date'])) ?>
            </td>
            <td class="articles-list__pd">
                <?= $article['last_name'] . ' ' . $article['first_name'] ?>
            </td>
            <? if ($article['isAccessed']): ?>
                <td>
                    <a class="btn" href="<?= ROOT ?>articles/edit/<?= $article['id_article'] ?>"><i
                                class="fa fa-pencil"></i></a>
                </td>
                <td>
                    <a class="btn js-btn-delete" href="<?= ROOT ?>articles/delete/<?= $article['id_article'] ?>"><i
                                class="fa fa-trash-o"></i></a>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
</table>
<a class="btn" href="<?= ROOT ?>articles/add">Создать статью</a>

