<div><?= nl2br($content) ?></div>
<div class="last-modified">Последние изменения: <?= $last_modified ?></div>
<? if ($isAccessed): ?>
    <div class="article-btns">
        <a class="btn" href="<?= ROOT ?>articles/edit/<?= $id_article ?>"><i class="fa fa-pencil"></i> Изменить статью</a>
        <a class="btn js-btn-delete" href="<?= ROOT ?>articles/delete/<?= $id_article ?>"><i class="fa fa-trash-o"></i> Удалить
            статью</a>
    </div>
<? endif; ?>