<table class="articles-list">
    <tr>
        <th>Название статьи</th>
        <th class="articles-list__pd">Дата публикации</th>
        <th class="articles-list__pd">Автор</th>
        <th></th>
        <th></th>
    </tr>
    <? foreach ($articles as $article) : ?>
        <tr>
            <td>
                <a href="<?=ROOT?>articles/article/<?= $article['id_article'] ?>"><?= $article['name'] ?></a>
            </td>
            <td class="articles-list__pd">
                <?= date('d.m.Y', strtotime($article['publication_date'])) ?>
            </td>
            <td class="articles-list__pd">
                <?= $article['last_name'] . ' ' . $article['first_name'] ?>
            </td>
        </tr>
    <? endforeach; ?>
</table>
