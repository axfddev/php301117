<?

namespace Models;

use Models\Helpers\QueryBuilder;
use Models\Helpers\Session;

class User extends Model
{
    protected $isAuth;
    protected $id;
    protected $login;
    protected $name;
    protected $lastName;
    protected $phone;
    protected $email;
    protected $password;

    public function __construct()
    {
        parent::__construct('users', 'id_user');
        $this->checkAuth();
    }

    public function checkAuth()
    {
        if (Session::instance()->getSession('auth')) {
            $this->isAuth = true;
        } elseif (isset($_COOKIE['UID'])) {
            $userId = Token::instance()->checkToken($_COOKIE['UID']);
            if ($userId) {
                $this->login($userId, $_COOKIE['UID']);
            }
        }
    }

    public function auth()
    {
        return $this->isAuth;
    }

    public function myHash($str)
    {
        $salt = 'myBlog2017';
        return hash('sha256', $str . $salt);
    }

    public function getOneUser($id)
    {
        $query = $this->qb->init()->join('user_groups')->joinOn(['id_group' => 'id_group'])->where(['id_user' => $id])->exec();
        $res = $query->query()->fetch();
        return (!$res['is_delete']) ? $res : false;
    }

    public function login($userId, $token = null, $rem = false)
    {
        $user = $this->getOneUser($userId);
        if ($user) {
            $token = $token ?? Token::instance()->setToken($user['id_user']);
            Session::instance()->setSession('auth', $token);
            Session::instance()->setSession('user', array(
                'id' => $user['id_user'],
                'name' => $user['first_name'] . ' ' . $user['last_name'],
                'access_level' => $user['access_level']
            ));
            if ($rem) {
                setcookie('UID', $token, time() + 3600 * 24 * 365, '/');
            }
        }
    }

    public function logout()
    {
        $token = Session::instance()->getSession('auth');
        Token::instance()->killToken($token);
        setcookie('UID', '', -1, '/');
        if (isset($_COOKIE['UID'])) unset($_COOKIE['UID']);
        Session::instance()->destroy();
    }

    public static function hasAccess($autorId)
    {
        $autorId = (int)$autorId;
        $user = Session::instance()->getSession('user');
        return $user['id'] == $autorId || $user['access_level'] > 20;
    }


    public function validationReg($repeatPassword)
    {
        $err = array(
            'login' => false,
            'password' => false,
            'repeat-password' => false,
            'name' => false
        );
        if (!strlen($this->login)) {
            $this->setError('Необходимо указать логин');
            $err['login'] = true;
        } elseif (!preg_match('/^[a-zA-Z0-9-]+$/', $this->login)) {
            $this->setError('Логин может содержать только латинские буквы, цифры и дефис');
            $err['login'] = true;
        } else {
            $query = $this->qb->init()->data(['id_user'])->where(['login' => $this->login, 'is_delete' => false])->exec() ;
            $arUsers = $query->query()->fetch();
            if ($arUsers['id_user']) {
                $this->setError('Указанный логин уже занят');
                $err['login'] = true;
            }
        }
        if (!strlen($this->name)) {
            $this->setError('Необходимо указать имя');
            $err['name'] = true;
        }
        if (!strlen($this->password)) {
            $this->setError('Необходимо указать пароль');
            $err['password'] = true;
        } elseif ($this->password !== $repeatPassword) {
            $this->setError('Введенные пароли не совпадают');
            $err['password'] = true;
            $err['repeat-password'] = true;
        }
        $err['SUCCSES'] = !count($this->err);
        return $err;
    }

    public function register()
    {
        $arRegData = [
            'login' => $this->login,
            'password' => $this->myHash($this->password),
            'uid' => $this->myHash($this->login . $this->myHash($this->password)),
            'first_name' => $this->name,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'email' => $this->email,
            'id_group' => 3
        ];
        return $this->qb->init('insert')->data($arRegData)->exec();
    }
}