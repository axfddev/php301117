<?

namespace Models;

use Models\Helpers\Session;
use Models\Traits\Singleton;

class Token extends Model
{
    use Singleton;
    public function __construct()
    {
        parent::__construct('auth', 'id');
    }

    public function checkToken($token)
    {
       $res = $this->qb->init()->where(['token' => $token])->exec()->query()->fetch();
       return $res['id_user'] ?? fslse;
    }

    public function setToken($userId)
    {
        $token = User::myHash($userId) . time();
        $res = $this->qb->init('insert')->data(['id_user' => $userId, 'token' => $token])->exec();
        return ($res->insetrId()) ? $token : false;
    }

    public function killToken($token)
    {
        $res = $this->qb->init('delete')->where(['token' => $token])->exec();
    }
}