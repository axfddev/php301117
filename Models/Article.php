<?

namespace Models;

use Models\Helpers\QueryBuilder;

class Article extends Model
{
    protected $id;
    protected $name;
    protected $content;

    public function __construct()
    {
        parent::__construct('articles', 'id_article');
        $this->id = '';
    }

    public function getAllArticles()
    {
        $query = $this->qb->init('select')->join('users')->joinOn(['id_author' => 'id_user'])->where(['is_delete' => false])->order(['publication_date' => 'DESC'])->exec();
        return $query->query()->fetchAll();
    }

    public function getOneArticle()
    {
        $query = $this->qb->init('select')->where(['id_article' => $this->id])->exec();
        $res = $query->query()->fetch();
        return (!$res['is_delete']) ? $res : false;
    }

    public function validation()
    {
        $err = array('name' => false, 'content' => false);

        if ($this->id) {
            $article = $this->getOneArticle();
            if ($article) {
                if (!User::hasAccess($article['id_author'])) {
                    $this->setError("Не достаточно прав на изменение указанной статьи");
                }
            } else {
                $this->setError("Статья, которую вы пытались изменить удалена или не существует");
            }
        }
        if (!strlen($this->name)) {
            $this->setError('Необходимо указать название статьи');
            $err['name'] = true;
        }
        if (!strlen($this->content)) {
            $this->setError('Необходимо заполнить содержание статьи');
            $err['content'] = true;
        }

        $err['succses'] = !count($this->err);
        return $err;
    }

    public function deleteArticle()
    {
        $article = $this->getOneArticle($this->id);
        if ($article) {
            $this->name = $article['name'];
            if (User::hasAccess($article['id_author'])) {
                $res = $this->qb->init('update')->data(['is_delete' => true])->where(['id_article' => $this->id])->exec();
                if ($res->error()) {
                    $this->setError($res->error());
                }
            } else {
                $this->setError('Не достаточно прав на удаление указанной статьи');
            }
        } else {
            $this->setError('Статья которую вы пытаетесь удалить не найдена');
        }
        return !count($this->err);
    }

    public function writeArticle()
    {
        $arParams = array(
            'id_author' => $_SESSION['user']['id'],
            'name' => $this->name,
            'content' => $this->content,
            'publication_date' => date('Y-m-d H:i:s')
        );
        return $this->qb->init('insert')->data($arParams)->exec();
    }

    public function reWriteArticle()
    {
        $arData = array(
            'name' => $this->name,
            'content' => $this->content,
        );
        return $this->qb->init('update')->data($arData)->where(['id_article' => $this->id])->exec()->error();
    }
}