<?

namespace Models;

use Models\Helpers\QueryBuilder;

abstract class Model
{
    protected $qb;
    protected $prKey;
    protected $err;

    public function __construct($table, $prKey)
    {
        $this->qb = new QueryBuilder($table);
        $this->prKey = $prKey;
        $this->err = [];
    }

    public function set($field, $value = '')
    {
        if (is_array($field)) {
            foreach ($field as $key => $item) {
                $this->set($key, $item);
            }
        } else {
            if (property_exists($this, $field)) {
                $this->$field = htmlspecialchars(trim($value)) ?? '';
            }
        }
    }

    public function get($field)
    {
        if (is_array($field)) {
            $res = [];
            foreach ($field as $key => $item) {
                $res[$key] = $this->get($key);
            }
        } else {
            return (property_exists($this, $field)) ? $this->$field : false;
        }
    }

    public function one($id)
    {
        return $this->qb->init('select')->where([$this->prKey => $id])->exec();
    }

    public function getSome($arWhere = [])
    {
        return $this->qb->init('select')->where($arWhere)->exec();
    }

    public function all($id)
    {
        return $this->qb->init('select')->exec();
    }

    public function setError($text, $add = true)
    {
        if ($text !== false) {
            if ($add) {
                $this->err[] = $text;
            } else {
                $this->err = [$text];
            }
        }
    }

    public function getError()
    {
        return implode(', ', $this->err);
    }
}