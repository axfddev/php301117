<?

namespace Models\Helpers;

use Models\Traits\Singleton as Singleton;

class Session
{
    use Singleton;

    protected function __construct()
    {
        session_start();
    }

    public function setSession($key, $value){
        $_SESSION[$key] = $value;
    }

    public function getSession($key){
        return $_SESSION[$key] ?? false;
    }

    public function destroy(){
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '',  -1, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }
        session_destroy();
    }
}