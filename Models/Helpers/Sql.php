<?php

namespace Models\Helpers;

use PDO;
use Models\Traits\Singleton as Singleton;

class Sql
{
    use Singleton;

    protected $db;
    protected $queryRes;

    protected function __construct()
    {
        $this->db = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
        $this->db->exec('SET NAMES UTF8');
        $this->queryRes = new QueryRes();
    }

    protected function checkError()
    {
        $this->lastError = false;
        $info = $this->queryRes->query()->errorInfo();
        if ($info[0] != PDO::ERR_NONE) {
            $this->queryRes->setlastError($info[2]);
        }
    }

    public function exec($sql, $params = [])
    {
        $this->queryRes->init();
        $this->queryRes->setQuery($this->db->prepare($sql));
//        echo '<pre>';
//        var_dump($this->queryRes);
//        echo '</pre>';
        $this->queryRes->query()->execute($params);
        $this->checkError();
        $this->queryRes->setlastInsetrId($this->db->lastInsertId());

        return $this->queryRes;
    }
}