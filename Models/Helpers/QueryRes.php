<?

namespace Models\Helpers;

class QueryRes
{
    protected $query;
    protected $lastError;
    protected $lastInsetrId;

    public function setQuery($query = null)
    {
        $this->query = $query;
    }

    public function setlastError($lastError = null)
    {
        $this->lastError = $lastError;
    }

    public function setlastInsetrId($lastInsetrId = null)
    {
        $this->lastInsetrId = $lastInsetrId;
    }

    public function init(){
        $this->setQuery();
        $this->setlastError();
        $this->setlastInsetrId();
    }

    public function query()
    {
        return $this->query;
    }

    public function error()
    {
        return $this->lastError;
    }

    public function insetrId()
    {
        return $this->lastInsetrId;
    }
}