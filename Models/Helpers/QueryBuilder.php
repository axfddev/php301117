<?

namespace Models\Helpers;

class QueryBuilder
{
    protected $table;
    protected $join;
    protected $joinOn;
    protected $params;
    protected $type;
    protected $data;
    protected $where;
    protected $whereConditions;
    protected $order;
    protected $limit;
    protected $sql;


    public function __construct($table)
    {
        $this->table = $table;
        $this->sql = Sql::instance();
    }

    public function data($arData = [])
    {
        $this->data = is_array($arData) ? $arData : [];
        return $this;
    }

    public function where($arWhere = [], $arWhereConditions = [])
    {
        $this->where = is_array($arWhere) ? $arWhere : [];
        $this->whereConditions = is_array($arWhereConditions) ? $arWhereConditions : [];
        return $this;
    }

    public function order($arOrder = [])
    {
        $this->order = is_array($arOrder) ? $arOrder : [];
        return $this;
    }

    public function limit($limit = false)
    {
        $this->limit = is_int($limit) ? $limit : false;
        return $this;
    }

    public function join($table)
    {
        $this->join = new Self($table);
        return $this;
    }

    public function joinOn($arOn = [])
    {
        $this->joinOn = is_array($arOn) ? $arOn : [];
        return $this;
    }

    public function joinData($arData = [])
    {
        $this->join->data($arData);
        return $this;
    }

    public function init($type = 'select')
    {
        $this->data();
        $this->where();
        $this->order();
        $this->limit();
        $this->joinOn();
        $this->join = null;

        $this->type = $type;
        return $this;
    }

    public function exec()
    {
        return $this->sql->exec($this->queryBuild(), $this->params);
    }


// Служебные методы

    protected function queryBuild()
    {


        switch ($this->type) {
            case 'insert':
                $this->params = $this->data;
                return $this->insert();
            case 'update':
                $this->params = array_merge($this->data, $this->where);
                return $this->update();
            case 'delete':
                $this->params = array_merge($this->data, $this->where);
                return $this->delete();
            default:
                $this->params = $this->where;
                return $this->select();
        }
    }


    //  Построители отдельных видов зарпосов

    protected function selectData()
    {
        if (count($this->data)) {
            $this->data = array_map(function ($item) {
                return "$this->table.$item";
            }, $this->data);
            $select = implode(', ', $this->data);
        } else {
            $select = "$this->table.*";
        };

        return $select;
    }

    protected function select()
    {
        $select = $this -> selectData();

        if ($this->join){
            $select .= ", " . $this->join->selectData();
            $joinText = " LEFT JOIN ". $this->join->table . " ON " . $this->joinOnBuild();
        }

        return "SELECT $select FROM $this->table" . $joinText. $this->whereBuild() . $this->orderBuild() . $this->limitBuild();
    }

    protected function update()
    {
        if (count($this->data)) {
            $arResult = [];
            foreach ($this->data as $key => $value) {
                $arResult[] = "$this->table.$key = :$key";
            }
            return "UPDATE $this->table SET " . (implode(', ', $arResult)) . $this->whereBuild();
        }
        return false;
    }

    protected function delete()
    {
        return "DELETE FROM $this->table" . $this->whereBuild() . $this->limitBuild();
    }

    protected function insert()
    {
        if (count($this->data)) {
            $keys = [];
            $masks = [];
            foreach ($this->data as $key => $value) {
                $keys[] = "$this->table.$key";
                $masks[] = ":$key";
            }
            return "INSERT INTO $this->table (" . implode(', ', $keys) . ") VALUES (" . implode(', ', $masks) . ")";
        }
        return false;
    }


    //  Вспомогательные построители

    protected function whereBuild()
    {
        $arItems = [' WHERE'];
        $isFirst = true;
        foreach ($this->where as $key => $value) {
            $addition = 'AND';
            $comparison = '=';

            if (array_key_exists($key, $this->whereConditions)) {
                if (is_array($this->whereConditions[$key])) {
                    $addition = $this->whereConditions[$key]['add'] ?? 'AND';
                    $comparison = $this->whereConditions[$key]['comp'] ?? '=';
                } else {
                    $comparison = $this->whereConditions[$key] ?? '=';
                }
            }
            $arItems[] = ($isFirst ? "" : "$addition ") . "$this->table.$key $comparison :$key";
            $isFirst = false;
        }
        return count($arItems) > 1 ? implode(" ", $arItems) : '';
    }

    protected function orderBuild()
    {
        if (count($this->order)) {
            $arItems = [];
            foreach ($this->order as $key => $item) {
                $item = ($item == 'DESC') ? $item : '';
                $arItems [] = "$this->table.$key $item";
            }
            return " ORDER BY " . implode(", ", $arItems);
        }
        return '';
    }

    protected function limitBuild()
    {
        return ($this->limit) ? " LIMIT $this->limit" : '';
    }

    protected function joinOnBuild()
    {
        if (count($this->joinOn)) {
            $arItems = [];
            foreach ($this->joinOn as $key => $item) {
                $arItems [] = "$this->table.$key = " . $this->join->table . ".$item";
            }
            return implode(", ", $arItems);
        }
        return '';
    }
}