<?
namespace Models\Traits;

trait Singleton
{
    protected static $singleton_instance;

    public static function instance(){
        if(self::$singleton_instance == null){
            self::$singleton_instance = new self();
        }

        return self::$singleton_instance;
    }
}