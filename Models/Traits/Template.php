<?php

namespace Models\Traits;

trait Template
{
    public function render($fname, $params = [], $printNow = false)
    {
        extract($params);
        ob_start();
        include("Views/{$fname}.php");
        if ($printNow) {
            echo ob_get_clean();
        } else {
            return ob_get_clean();
        }
    }

    public function gotoPage($page)
    {
        header("Location: $page", true, 307);
        exit();
    }
}

