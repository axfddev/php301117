<?php

namespace Models;

class Routing
{
    protected $params;
    protected $controller;
    protected $action;
    protected $controllers;
    protected $subNameSpaces;

    public function __construct($queryurl = '')
    {
        $paramsTmp = explode('/', $queryurl ?? '');
        $this->params = [];
        foreach ($paramsTmp as $p) {
            if ($p !== '') {
                $this->params[] = $p;
            }
        }

        $this->controller = ucfirst($this->params[0] ?? 'articles');
        $this->action = 'action_' . ($this->params[1] ?? 'index');
        $this->subNameSpaces = ['Admin', 'Client'];
        $this->setCtrls();
    }

    protected function setCtrls()
    {
        $this->controllers = [];
        foreach ($this->subNameSpaces as $sNS) {
            $className = "\Controllers\\$sNS\\$this->controller";
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . ROOT . str_replace('\\', '/', $className) . '.php')) {
                $ctrl = new $className();
                if (method_exists($ctrl, $this->action)) {
                    $this->controllers[$sNS] = $ctrl;
                }
            }
        }
    }

    protected function getCtrl()
    {
        $success = true;
        if (count($this->controllers) === 0) {
            $ctrl = new \Controllers\Client\Articles;
            $ctrl->set404();
            $success = false;
        } elseif (count($this->controllers) === 1) {
            $ctrl = array_shift($this->controllers);
            if (method_exists($ctrl, 'user')) {
                if (!$ctrl->user()->auth()) {
                    $ctrl->action_login();
                    $success = false;
                }
            }
        } else {
            if ($this->controllers['Admin']->user()->auth()) {
                $ctrl = $this->controllers['Admin'];
            } else {
                $ctrl = $this->controllers['Client'];
            }
        }

        if ($success) {
            $ctrl->load($this->params);
            $action = $this->action;
            $ctrl->$action();
        }
        return $ctrl;
    }

    public function render()
    {
        $this->getCtrl()->mainRender();
    }
}

