<?php

namespace Controllers\Client;

use Models\Template;

class Client extends \Controllers\Base
{
    public function __construct()
    {
        parent::__construct();
        $this->title = '';
        $this->content = '';
    }

    public function mainRender()
    {
        return $this->render('v_main', [
            'title' => $this->title,
            'content' => $this->content
        ], true);
    }
}