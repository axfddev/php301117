<?php

namespace Controllers\Client;

use Models\Template;
use Models\User;

class Users extends Client
{
    protected $user;


    public function __construct()
    {
        parent::__construct();
        $this->user = new User($this->sql);
    }

    public function action_registration()
    {
        $arParams = [];
        if (count($_POST) > 0) {
            $this->user->set([
                'login' => $_POST['login'],
                'name' => $_POST['name'],
                'lastName' => $_POST['last-name'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'password' => $_POST['password']
            ]);
            $repeatPassword = htmlspecialchars(trim($_POST['repeat-password'])) ?? '';

            $arRes = $this->user->validationReg($repeatPassword);
            if ($arRes['SUCCSES']) {
                $res = $this->user->register();
                if (!$res->error()) {
                    $this->user->login($res->insetrId());
                    $this->gotoPage($_SESSION['ref']);
                } else {
                    $this->user->setError($res->error());
                }
            }
        }
        $arRes['text'] = $this->user->getError();
        $arParams = [
            'login' => $this->user->get('login'),
            'password' => $this->user->get('password'),
            'name' => $this->user->get('name'),
            'lastName' => $this->user->get('lastName'),
            'phone' => $this->user->get('phone'),
            'email' => $this->user->get('email'),
            'repeatPassword' => $repeatPassword,
            'err' => $arRes,
        ];

        $this->title = "Регистрация";
        $this->content = $this->render('v_registration_form', $arParams);
    }
}

