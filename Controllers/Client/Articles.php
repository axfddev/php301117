<?php

namespace Controllers\Client;

use Models\Template;
use Models\Article;

class Articles extends Client
{
    protected $article;

    public function __construct()
    {
        parent::__construct();
        $this->article = new Article($this->sql);
    }

    public function action_index()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        $this->title = 'Публичный блог';
        $this->content = $this->render('v_client_index', [
            'articles' => $this->article->getAllArticles()
        ]);
    }

    public function action_article()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        $this->article->set('id', $this->params[2]);
        $articleRes = $this->article->getOneArticle();
        if ($articleRes === false) {
            $this->set404();
        } else {
            $this->title = $articleRes['name'];
            $this->content = $this->render('v_client_post', $articleRes);
        }
    }
}