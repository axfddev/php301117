<?php

namespace Controllers\Admin;

use Models\Template;
use Models\User;

class Admin extends \Controllers\Base
{
    protected $title;
    protected $content;
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->title = '';
        $this->content = '';
        $this->user = new User();
    }

    public function user()
    {
        return $this->user;
    }

    public function action_login()
    {
        if (count($_POST) > 0) {
            $this->user->set([
                'login' => $_POST['login'],
                'password' => $_POST['password']
            ]);
            $userRes = $this->user->getSome(['login'=>$this->user->get('login')])->query()->fetch();
            $userId = $userRes['id_user'];
            if ($userId) {
                $this->user->login($userId, null, isset($_POST['remember']));
                $this->gotoPage($_SESSION['ref']);
            } else {
                $this->user->setError('Указан некорректный логин/пароль!');
            }
        } elseif (isset($_GET['logout'])) {
            $path = $_SESSION['ref'];
            $this->user->logout();
            $this->gotoPage($path);
        }
        $this->title = "Авторизация";
        $this->content = $this->render('v_login_form', ['error' => $this->user->getError()]);
    }

    public function mainRender()
    {
        return $this->render('v_main', [
            'title' => $this->title,
            'content' => $this->content
        ], true);
    }
}