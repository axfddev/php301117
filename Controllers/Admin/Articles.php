<?php

namespace Controllers\Admin;

use Models\Template;
use Models\Article;
use Models\User;

class Articles extends Admin
{
    protected $article;

    public function __construct()
    {
        parent::__construct();
        $this->article = new Article($this->sql);
    }

    public function action_index()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        $arArticles = $this->article->getAllArticles();
        foreach ($arArticles as &$article) {
            $article['isAccessed'] = User::hasAccess($article['id_author']);
        }

        $this->title = 'Публичный блог';
        $this->content = $this->render('v_admin_index', [
            'articles' => $arArticles,
        ]);
    }

    public function action_article()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        $this->article->set('id', $this->params[2]);
        $articleRes = $this->article->getOneArticle();
        if ($articleRes === false) {
            $this->set404();
        } else {
            $articleRes['isAccessed'] = User::hasAccess($articleRes['id_author']);
            $this->title = $articleRes['name'];
            $this->content = $this->render('v_admin_post', $articleRes);
        }
    }

    public function action_add()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        if (count($_POST) > 0) {
            $this->article->set([
                'name' => $_POST['name'],
                'content' => $_POST['content']
            ]);
            $arErr = $this->article->validation();
            if ($arErr['succses']) {
                $res = $this->article->writeArticle();
                if ($res->error()) {
                    $this->article->setError($res->error());
                } else {
                    $this->gotoPage(ROOT . 'articles/article/' . $res->insetrId());
                }
            }
        }
        $arErr['text'] = $this->article->getError();
        $arParams = [
            'name' => $this->article->get('name'),
            'content' => $this->article->get('content'),
            'err' => $arErr
        ];
        $this->title = "Добавление новой статьи" . ($arParams['name'] ? " \"{$arParams['name']}\"." : ".");
        $this->content = $this->render('v_article_form', $arParams);
    }

    public function action_edit()
    {
        $_SESSION['ref'] = $_SERVER['REQUEST_URI'];
        if (count($_POST) > 0) {
            if (isset($_POST['articleId'])) {
                $this->article->set([
                    'id' => $_POST['articleId'],
                    'name' => $_POST['name'],
                    'content' => $_POST['content']
                ]);
                $arErr = $this->article->validation();
                if ($arErr['succses']) {
                    $resErr = $this->article->reWriteArticle();
                    if ($resErr) {
                        $this->article->setError($resErr);
                        $shPage = true;
                    } else {
                        $this->gotoPage(ROOT . 'articles/article/' . $this->article->get('id'));
                    }
                } else {
                    $shPage = true;
                }
            }
        } else {
            $this->article->set('id', $this->params[2]);
            $articleRes = $this->article->getOneArticle();
            if ($articleRes) {
                if (User::hasAccess($articleRes['id_author'])) {
                    $shPage = true;
                    $this->article->set([
                        'name' => $_POST['name'],
                        'content' => $_POST['content']
                    ]);
                } else {
                    $this->gotoPage(ROOT . 'articles/article/' . $this->article->get('id'));
                }
            }
        }
        $arErr['text'] = $this->article->getError();
        $arParams = [
            'id_article' => $this->article->get('id'),
            'name' => $this->article->get('name'),
            'content' => $this->article->get('content'),
            'err' => $arErr
        ];
        if ($shPage) {
            $this->title = "Изменение статьи: " . $arParams['name'];
            $this->content = $this->render('v_article_form', $arParams);
        } else {
            $this->set404();
        }
    }

    public function action_delete()
    {
        if (count($_POST) > 0) {
            if (isset($_POST['ajax'])) {
                $this->article->set('id', $this->params[2]);
                $articleRes = $this->article->getOneArticle();
                if ($articleRes) {
                    if (User::hasAccess($articleRes['id_author'])) {
                        $arParams = [
                            'id_article' => $this->article->get('id'),
                            'question' => "Вы действительно хотите удалить статью: \"{$articleRes['name']}\"?",
                            'action' => ROOT . 'articles/delete/',
                        ];
                        $template = 'v_delete_form';
                    } else {
                        $arParams ['error'] = 'Не достаточно прав на удаление указанной статьи';
                        $template = 'v_delete_err';
                    }
                } else {
                    $arParams ['error'] = 'Статья которую вы пытаетесь удалить не найдена';
                    $template = 'v_delete_err';
                }
                $this->render($template, $arParams, true);
                exit();
            } else {
                if (isset($_POST['id']) && $_POST['confirm'] === 'Да') {
                    $this->article->set('id', $_POST['id']);
                    $deletRes = $this->article->deleteArticle();
                    if ($deletRes) {
                        $this->gotoPage(ROOT);
                    } else {
                        $this->title = "Удаление статьи: " . $this->article->get('name');
                        $this->content = $this->render('v_delete', ['error' => $this->article->getError()]);
                    }
                } else {
                    $this->gotoPage($_SESSION['ref']);
                }
            }
        } else {
            $this->set404();
        }
    }
}

