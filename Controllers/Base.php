<?php

namespace Controllers;

use Models\Traits\Template;
use Models\Helpers\Sql;

abstract class Base
{
    use Template;

    protected $title;
    protected $content;
    protected $params;
    protected $sql;

    public function __construct()
    {
        $this->sql = Sql::instance();
    }

    public function load($params)
    {
        $this->params = $params;
    }

    public function set404($text = "Увы, как мы не искали, страницу так и не нашли...")
    {
        header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");
        $this->title = 'Страница не найдена';
        $this->content = $this->render('v_404', ['text' => $text]);
    }

    public abstract function mainRender();
}