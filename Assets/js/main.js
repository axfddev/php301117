$(function () {
    $('body').on('focus', '.js-form-item', function () {
        var $this = $(this);
        $this.removeClass('error');
    });

    $('body').on('click', '.js-btn-delete', function (e) {
        e.preventDefault();
        $.post($(this).attr('href'), {ajax: "true"}, function (data) {
                $(".js-modal-content").html(data);
                $(".js-modal").fadeIn(300);
            }
        );
    });

    $('body').on('click', '.js-cancel', function (e) {
        e.preventDefault();
        $(this).closest('.js-modal').fadeOut(300, function () {
            $(this).find(".js-modal-content").html('');
        });
    });


});